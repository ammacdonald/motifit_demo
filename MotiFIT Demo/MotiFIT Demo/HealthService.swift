//
//  AppleHealthService.swift
//  MotiFIT Demo
//
//  Created by Aaron MacDonald on 2017-04-30.
//  Copyright © 2017 amacdonald. All rights reserved.
//

import Foundation
import HealthKit

class HealthService
{
    let health: HKHealthStore = HKHealthStore()
    let heartRateUnit:HKUnit = HKUnit(from: "count/min")
    let heartRateType:HKQuantityType   = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
    var heartRateQuery:HKSampleQuery?
    let healthStore = HKHealthStore()
    
    /*
     Create mock heart rate samples and save them to the health store.
     */
    private func saveMockHeartRateSamples(period: (yesterdaysDate: Date, todaysDate: Date), completion: @escaping () -> Void) {
        var samples = [HKQuantitySample]()
        
        for i in 0..<25 {
            let heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)
            let heartRateQuantity = HKQuantity(unit: HKUnit(from: "count/min"),
                                               doubleValue: Double(arc4random_uniform(80) + 100))
            let calendar = NSCalendar.current
            var dateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: period.yesterdaysDate)
            dateComponents.day = Int(arc4random_uniform(2)) + dateComponents.day!
            dateComponents.hour = Int(arc4random_uniform(24))
            dateComponents.minute = Int(arc4random_uniform(60))
            dateComponents.second = Int(arc4random_uniform(60))
            var sampleDate = calendar.date(from: dateComponents)
            
            let heartSample = HKQuantitySample(type: heartRateType!,
                                               quantity: heartRateQuantity, start: sampleDate!, end: sampleDate!)

            samples.append(heartSample)
        }
        self.healthStore.save(samples, withCompletion: { (success, error) -> Void in
            completion()
        })
    }
    
    /*
     Request heart rate samples from the health store.
     */
    private func requestHeartRateSamples(startDate: Date, endDate: Date, completion: @escaping ([HeartRateSample]) -> Void)
    {
        let heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate)
        let sortDescriptors = [
            NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        ]
        var heartRateSamples = [HeartRateSample]();
        let heartRateQuery = HKSampleQuery(sampleType: heartRateType, predicate: predicate, limit: 25, sortDescriptors: sortDescriptors) { (query, results, error) in
            for item in results! {
                let sample = item as! HKQuantitySample
                heartRateSamples.append(HeartRateSample(heartRate: sample.quantity.doubleValue(for: self.heartRateUnit), startDate: sample.startDate, endDate: sample.endDate))
            }
            completion(heartRateSamples);
        }
        health.execute(heartRateQuery)
        
    }
    
    /*
     Return a tuple with yesterday and today's dates.
     */
    private func getYesterdayAndTodaysDates() -> (yesterdaysDate: Date, todaysDate: Date)
    {
        let calendar = NSCalendar.current
        let todaysDate = NSDate()
        var yesterdaysDate = calendar.date(byAdding: .day, value: -1, to: todaysDate as Date)
        var yesterdaysDateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: yesterdaysDate!)
        yesterdaysDateComponents.hour = 0
        yesterdaysDateComponents.minute = 0
        yesterdaysDateComponents.second = 0
        yesterdaysDate = calendar.date(from: yesterdaysDateComponents)
        return (yesterdaysDate!, todaysDate as Date)
    }

    
    /*
     Callback with a list of heart rate samples.
     */
    public func getHeartRateSamples(completion: @escaping ([HeartRateSample]) -> Void)
    {
        let allTypes = Set([HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!])
        healthStore.requestAuthorization(toShare: allTypes, read: allTypes) { (success, error) in
            let dates = self.getYesterdayAndTodaysDates()
            self.saveMockHeartRateSamples(period: dates, completion: {
                self.requestHeartRateSamples(startDate: dates.yesterdaysDate, endDate: dates.todaysDate, completion: { (samples: [HeartRateSample]) in
                    completion(samples);
                })
            })
        }
    }
}
