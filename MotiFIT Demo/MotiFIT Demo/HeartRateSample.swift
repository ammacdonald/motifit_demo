//
//  HeartRateSample.swift
//  MotiFIT Demo
//
//  Created by Aaron MacDonald on 2017-05-01.
//  Copyright © 2017 amacdonald. All rights reserved.
//

import Foundation

struct HeartRateSample
{
    var heartRate : Double
    var startDate : Date
    var endDate : Date
    init(heartRate: Double, startDate : Date, endDate : Date) {
        self.heartRate = heartRate
        self.startDate = startDate
        self.endDate  = endDate
    }
}
