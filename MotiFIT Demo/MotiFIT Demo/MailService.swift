//
//  MailService.swift
//  MotiFIT Demo
//
//  Created by Aaron MacDonald on 2017-04-30.
//  Copyright © 2017 amacdonald. All rights reserved.
//

import Foundation
import Alamofire

class MailService
{
    let urlString = "https://api:key-79bacb34d98f83cc93e86ff3c60ef521@api.mailgun.net/v3/sandbox53d19d37fd6244f18ceb24260cc70e8c.mailgun.org/messages"
    /* 
        Send a message to the given email address using MailGun
     */
    public func Send(to: String, from: String, subject: String, message: String)
    {
        let parameters: Parameters = [
            "from": from,
            "to": to,
            "subject": subject,
            "text": message
        ]
        let request = Alamofire.request(urlString, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
    }
}
