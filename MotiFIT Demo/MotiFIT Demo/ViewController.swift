//
//  ViewController.swift
//  MotiFIT Demo
//
//  Created by Aaron MacDonald on 2017-04-30.
//  Copyright © 2017 amacdonald. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let mailService = MailService()
    let healthService = HealthService()
    let from = "Mailgun Sandbox <postmaster@sandbox53d19d37fd6244f18ceb24260cc70e8c.mailgun.org>"
    let subject = "Heart Beat Report"
    
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func sendHeartBeatReport(completion: @escaping () -> Void) {
        healthService.getHeartRateSamples() { (samples: [HeartRateSample]) in
            var message = String()
            for sample in samples
            {
                message.append("Date: \(sample.startDate) Heart rate:\(sample.heartRate)\n")
            }
            self.mailService.Send(to: self.emailTextField.text!, from: self.from, subject: self.subject, message: message)
            completion()
        }
    }
    
    @IBAction func onSendEmail(_ sender: Any) {
        sendHeartBeatReport {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Heart Rate Report", message: "Message sent!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

}

